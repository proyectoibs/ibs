const { Given, When, Then } = require('cucumber');
const assert = require('assert');
const { driver } = require('../support/web_driver');

Given(/^Browse to web site "([^"]*)"$/, async function(url) {
    await driver.get(url);

});


When(/^input keyword "([^"]*)"$/, async function (keyword) {
    return driver.findElement({ id: "Usuario" }).sendKeys(keyword);
   
});   

When(/^input text "([^"]*)"$/, async function (text) {
    return driver.findElement({ id: "Clave" }).sendKeys(text);

});

Then(/^click Search button$/, async function () {
    return driver.findElement({ id: "btnEnviar" }).click();
});

Then(/^search result should contain "([^"]*)"$/, async function (keyword) {
    await driver.sleep(1000);
    let result = await driver.findElement({ id: "horario" }).getText();
    return assert.ok(result.includes(keyword));
});