select * from comuna

--- CONSULTA REGION ---
select * from region;
--- CONSULTA COMUNA ---
select c.id_comuna, c.nombre, r.nombre as "NOMBRE REGION" from comuna c inner join
dbo.region r on r.id_region =  c.REGION_id_region;
--- CONSULTA TIPO_DEPENDENCIA---
select * from TIPO_DEPENDENCIA
--- CONSULTA TIPO_NIVEL_ATENCION ---
select * from TIPO_NIVEL_ATENCION

--- CONSULTA ESTABLECIMIENTO ----
select e.id_codigo_nuevo, e.id_codigo_antiguo, e.nombre_oficial, c.nombre as "NOMBRE COMUNA",r.ID_REGION as "CODIGO REGION",
 c.ID_COMUNA as "CODIGO COMUNA", r.NOMBRE as "NOMBRE REGION",n.nombre AS "NIVEL DE ATENCION", d.nombre AS "DEPENDENCIA" from ESTABLECIMIENTO e INNER JOIN
 dbo.comuna c on c.id_comuna = e.COMUNA_id_comuna INNER JOIN dbo.region r on r.id_region = c.REGION_id_region INNER JOIN 
 dbo.TIPO_NIVEL_ATENCION n on n.id_nivel = e.TIPO_NIVEL_ATENCION_id_nivel INNER JOIN
 dbo.TIPO_DEPENDENCIA d on d.id_dependencia = e.TIPO_DEPENDENCIA_id_dependencia;
  
--- CONSULTA SERIE --- 
select * from serie;

--- CONSULTA PRESTACION ---
select p.id_prestacion as "CODIGO", p.glosa, p.hoja, p.linea, p.ano, s.nombre as "NOMBRE SERIE" from prestacion p INNER JOIN
serie s on s.id_serie = p.SERIE_id_serie;

--- Año 2018 hay un total de 6137 prestaciones sin contar con las PRES_BS que se encuentran repetidas (11 prestaciones) ---

--- CONSULTA REM 1.1---
select * from rem;

--- CONSUTAL REM 1.2 ---
select * from rem;

select r.mes, r.id_servicio, r.ano, e.id_codigo_nuevo, p.id_prestacion, r.fecha_ingreso, r.col_01, r.col_02, r.col_03, r.col_04
, r.col_05, r.col_06, r.col_07, r.col_08, r.col_09, r.col_10, r.col_11, r.col_12, r.col_13, r.col_14, r.col_15, r.col_16, r.col_17
, r.col_18, r.col_19, r.col_20, r.col_21, r.col_22, r.col_23, r.col_24, r.col_25, r.col_26, r.col_27, r.col_28, r.col_29, r.col_30
, r.col_31, r.col_32, r.col_33, r.col_34, r.col_35, r.col_36, r.col_37, r.col_38, r.col_39, r.col_40, r.col_41, r.col_42, r.col_43
, r.col_44, r.col_45, r.col_46, r.col_47, r.col_48, r.col_49, r.col_50, e.nombre_oficial AS "NOMBRE ESTABLECIMIENTO", c.nombre as "COMUNA",
P.glosa as "PRESTACION", p.hoja as "REM", p.linea as "LINEA" FROM REM r INNER JOIN
 dbo.ESTABLECIMIENTO e on e.id_codigo_nuevo = r.ESTABLECIMIENTO_id_codigo_nuevo INNER JOIN 
 dbo.PRESTACION p on p.id_prestacion = r.PRESTACION_id_prestacion INNER JOIN 
 dbo.COMUNA c on c.id_comuna = e.COMUNA_id_comuna;


--- CONSULTAS REM 2014 ---
Select * from BD_SA_2014
Select * from BD_SBM_2014
Select * from BD_SBS_2014
Select * from BD_SD_2014
Select * from BD_SP_2014

--- CONSULTAS REM 2015 ---
Select * from BD_SA_2015
Select * from BD_SBM_2015
Select * from BD_SBS_2015
Select * from BD_SD_2015
Select * from BD_SP_2015

--- CONSULTAS REM 2016 ---
Select * from BD_SA_2016
Select * from BD_SBM_2016
Select * from BD_SBS_2016
Select * from BD_SD_2016
Select * from BD_SP_2016

--- CONSULTAS REM 2017 ---
Select * from BD_SA_2017
Select * from BD_SBM_2017
Select * from BD_SBS_2017
Select * from BD_SD_2017
Select * from BD_SP_2017

--- CONSULTAS REM 2018 ---
Select * from BD_SA_2018
Select * from BD_SBM_2018
Select * from BD_SBS_2018
Select * from BD_SD_2018
Select * from BD_SP_2018

