
-- -----------------------------------------------------
-- Table  BD_REM . REGION 
-- -----------------------------------------------------
CREATE TABLE REGION  (
   id_region  INT NOT NULL,
   nombre  VARCHAR(50) NOT NULL,
  PRIMARY KEY ( id_region ))


-- -----------------------------------------------------
-- Table  BD_REM . COMUNA 
-- -----------------------------------------------------
CREATE TABLE COMUNA  (
   id_comuna  VARCHAR(50) NOT NULL,
   nombre  VARCHAR(50) NOT NULL,
   REGION_id_region  INT NOT NULL,
  PRIMARY KEY ( id_comuna ),
  CONSTRAINT  fk_COMUNA_REGION 
    FOREIGN KEY ( REGION_id_region )
    REFERENCES  REGION  ( id_region ))

-- -----------------------------------------------------
-- Table  BD_REM . TIPO_NIVEL_ATENCION 
-- -----------------------------------------------------
CREATE TABLE TIPO_NIVEL_ATENCION  (
   id_nivel  INT IDENTITY(1,1) NOT NULL,
   nombre  VARCHAR(50) NOT NULL,
  PRIMARY KEY ( id_nivel ))


-- -----------------------------------------------------
-- Table  BD_REM . TIPO_DEPENDENCIA 
-- -----------------------------------------------------
CREATE TABLE TIPO_DEPENDENCIA  (
   id_dependencia  INT IDENTITY(1,1) NOT NULL,
   nombre  VARCHAR(50) NOT NULL,
  PRIMARY KEY ( id_dependencia ))


-- -----------------------------------------------------
-- Table  BD_REM . ESTABLECIMIENTO 
-- -----------------------------------------------------
CREATE TABLE ESTABLECIMIENTO  (
   id_codigo_nuevo  INT NOT NULL,
   id_codigo_antiguo  VARCHAR(50) NOT NULL,
   nombre_oficial  VARCHAR(200) NOT NULL,
   COMUNA_id_comuna VARCHAR(50) NOT NULL,
   TIPO_NIVEL_ATENCION_id_nivel  INT NOT NULL,
   TIPO_DEPENDENCIA_id_dependencia  INT NOT NULL,
  PRIMARY KEY ( id_codigo_nuevo ),
  CONSTRAINT  fk_ESTABLECIMIENTO_COMUNA1 
    FOREIGN KEY ( COMUNA_id_comuna )
    REFERENCES  COMUNA  ( id_comuna ),
  CONSTRAINT  fk_ESTABLECIMIENTO_TIPO_NIVEL_ATENCION1 
    FOREIGN KEY ( TIPO_NIVEL_ATENCION_id_nivel )
    REFERENCES  TIPO_NIVEL_ATENCION  ( id_nivel ),
  CONSTRAINT  fk_ESTABLECIMIENTO_TIPO_DEPENDENCIA1 
    FOREIGN KEY ( TIPO_DEPENDENCIA_id_dependencia )
    REFERENCES  TIPO_DEPENDENCIA  ( id_dependencia ))


-- -----------------------------------------------------
-- Table  BD_REM . SERIE 
-- -----------------------------------------------------
CREATE TABLE SERIE  (
   id_serie  INT IDENTITY(1,1) NOT NULL,
   nombre  VARCHAR(50) NOT NULL,
  PRIMARY KEY ( id_serie ))


-- -----------------------------------------------------
-- Table  BD_REM . PRESTACION 
-- -----------------------------------------------------
CREATE TABLE PRESTACION  (
   id_prestacion  VARCHAR(50) NOT NULL,
   glosa  VARCHAR(400) NOT NULL,
   hoja  VARCHAR(10) NOT NULL,
   linea  INT NOT NULL,
   ano  INT NOT NULL,
   SERIE_id_serie  INT NOT NULL,
  PRIMARY KEY ( id_prestacion ),
  CONSTRAINT  fk_PRESTACION_SERIE1 
    FOREIGN KEY ( SERIE_id_serie )
    REFERENCES  SERIE  ( id_serie ))


-- -----------------------------------------------------
-- Table  BD_REM . REM 
-- -----------------------------------------------------
CREATE TABLE REM  (
   id_rem  INT IDENTITY(1,1) NOT NULL,
   mes  INT NOT NULL,
   id_servicio  INT NOT NULL,
   ano  INT NOT NULL,
   ESTABLECIMIENTO_id_codigo_nuevo  INT NOT NULL,
   PRESTACION_id_prestacion  VARCHAR(50) NOT NULL,
   fecha_ingreso  DATE NOT NULL,
   col_01  INT NULL,
   col_02  INT NULL,
   col_03  INT NULL,
   col_04  INT NULL,
   col_05  INT NULL,
   col_06  INT NULL,
   col_07  INT NULL,
   col_08  INT NULL,
   col_09  INT NULL,
   col_10  INT NULL,
   col_11  INT NULL,
   col_12  INT NULL,
   col_13  INT NULL,
   col_14  INT NULL,
   col_15  INT NULL,
   col_16  INT NULL,
   col_17  INT NULL,
   col_18  INT NULL,
   col_19  INT NULL,
   col_20  INT NULL,
   col_21  INT NULL,
   col_22  INT NULL,
   col_23  INT NULL,
   col_24  INT NULL,
   col_25  INT NULL,
   col_26  INT NULL,
   col_27  INT NULL,
   col_28  INT NULL,
   col_29  INT NULL,
   col_30  INT NULL,
   col_31  INT NULL,
   col_32  INT NULL,
   col_33  INT NULL,
   col_34  INT NULL,
   col_35  INT NULL,
   col_36  INT NULL,
   col_37  INT NULL,
   col_38  INT NULL,
   col_39  INT NULL,
   col_40  INT NULL,
   col_41  INT NULL,
   col_42  INT NULL,
   col_43  INT NULL,
   col_44  INT NULL,
   col_45  INT NULL,
   col_46  INT NULL,
   col_47  INT NULL,
   col_48  INT NULL,
   col_49  INT NULL,
   col_50  INT NULL,
  PRIMARY KEY ( id_rem ),
  CONSTRAINT  fk_REM_ESTABLECIMIENTO1 
    FOREIGN KEY ( ESTABLECIMIENTO_id_codigo_nuevo )
    REFERENCES  ESTABLECIMIENTO  ( id_codigo_nuevo ),
  CONSTRAINT  fk_REM_PRESTACION1 
    FOREIGN KEY ( PRESTACION_id_prestacion )
    REFERENCES  PRESTACION  ( id_prestacion ))

