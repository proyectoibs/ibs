--- INSERT REGION ---

INSERT INTO REGION (id_region, nombre)
VALUES (13, 'Metropolitana de Santiago');

INSERT INTO REGION (id_region, nombre)
VALUES (5, 'De Valparaíso');


--- INSERT COMUNA
select * from COMUNA;
delete from COMUNA where id_comuna=05201;



INSERT INTO COMUNA (id_comuna, nombre, REGION_id_region)
VALUES ('05201' ,'Isla de Pascua', 5);

INSERT INTO COMUNA (id_comuna, nombre, REGION_id_region)
VALUES ('13113' ,'La Reina', 13);

INSERT INTO COMUNA (id_comuna, nombre, REGION_id_region)
VALUES ('13114' ,'Las Condes', 13);

INSERT INTO COMUNA (id_comuna, nombre, REGION_id_region)
VALUES ('13115' ,'Lo Barnechea', 13);

INSERT INTO COMUNA (id_comuna, nombre, REGION_id_region)
VALUES ('13118' ,'Macul', 13);

INSERT INTO COMUNA (id_comuna, nombre, REGION_id_region)
VALUES ('13120' ,'Ñuñoa', 13);

INSERT INTO COMUNA (id_comuna, nombre, REGION_id_region)
VALUES ('13122' ,'Peñalolén', 13);

INSERT INTO COMUNA (id_comuna, nombre, REGION_id_region)
VALUES ('13123' ,'Providencia', 13);

INSERT INTO COMUNA (id_comuna, nombre, REGION_id_region)
VALUES ('13132' ,'Vitacura', 13);


--- INSERT SERIE ---
SELECT * FROM SERIE; 

Update SERIE Set nombre='SERIE A1' Where nombre='xxx'


INSERT INTO SERIE (nombre)
VALUES ('SERIE A1');

INSERT INTO SERIE (nombre)
VALUES ('SERIE BM');

INSERT INTO SERIE (nombre)
VALUES ('SERIE BS');

INSERT INTO SERIE (nombre)
VALUES ('SERIE D');

INSERT INTO SERIE (nombre)
VALUES ('SERIE P');

--- Copiar todos los datos de una tabla a otra -----
INSERT INTO tablaDestino 
SELECT * FROM tablaOrigen

INSERT INTO PRESTACION 
SELECT * FROM dbo.PRES_SP$

Update PRESTACION2 Set SERIE= 5 Where SERIE= 'SERIE P'
Update PRESTACION2 Set SERIE= 4 Where SERIE= 'SERIE D'
Update PRESTACION2 Set SERIE= 3 Where SERIE= 'SERIE BS'
Update PRESTACION2 Set SERIE= 2 Where SERIE= 'SERIE BM'
Update PRESTACION2 Set SERIE= 1 Where SERIE= 'SERIE A1'

(Codigos que se repiten en la PRES_SBS : 99000690 - 99000700 - 17200400 
 y se repite entre PRES_SBS y el PRES_SA : '01010101', '01010103', 01010201, 01010401, 01010601, 01010603, 01010901, 01010903)



--- INSERT TIPO_NIVEL_ATENCION
SELECT * FROM TIPO_NIVEL_ATENCION;

INSERT INTO TIPO_NIVEL_ATENCION (NOMBRE)
VALUES ('No Aplica');

INSERT INTO TIPO_NIVEL_ATENCION (NOMBRE)
VALUES ('Primario');

INSERT INTO TIPO_NIVEL_ATENCION (NOMBRE)
VALUES ('Secundario');

INSERT INTO TIPO_NIVEL_ATENCION (NOMBRE)
VALUES ('Terciario');



--- INSERT TIPO_DEPENDENCIA
SELECT * FROM TIPO_DEPENDENCIA;

INSERT INTO TIPO_DEPENDENCIA (NOMBRE)
VALUES ('Servicio de Salud');

INSERT INTO TIPO_DEPENDENCIA (NOMBRE)
VALUES ('Autoridad Sanitaria');

INSERT INTO TIPO_DEPENDENCIA (NOMBRE)
VALUES ('Establecimiento Experimental');

INSERT INTO TIPO_DEPENDENCIA (NOMBRE)
VALUES ('Municipal');



--- INSERT ESTABLECIMIENTO
INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112010, '12-010','Actividades gestionadas por la Dirección del Servicio para apoyo de la Red (S.S Metropolitano Oriente)','13123', 1, 1);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112011, '12-011','PRAIS (S.S Metropolitano Oriente)','13123', 1, 1);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112095, '12-095','Vacunatorio Internacional SEREMI de Salud Metropolitana','13123', 2, 2);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112100, '12-100','Hospital Del Salvador (Santiago, Providencia)','13123', 4, 1);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112101, '12-101','Hospital Dr. Luis Tisné B. (Santiago, Peñalolén)','13122', 4, 1);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112102, '12-102','Hospital de Niños Dr. Luis Calvo Mackenna (Santiago, Providencia)','13123', 4, 1);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112103, '12-103','Instituto Nacional de Enfermedades Respiratorias y Cirugía Torácica','13123', 4, 1);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112104, '12-104','Instituto de Neurocirugía Dr. Alfonso Asenjo','13123', 4, 1);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112105, '12-105','Instituto Nacional de Rehabilitación Infantil Presidente Pedro Aguirre Cerda','13122', 4, 1);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112106, '12-106','Instituto Nacional Geriátrico Presidente Eduardo Frei Montalva','13123', 4, 1);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112107, '12-107','Hospital Hanga Roa (Isla De Pascua)','05201', 4, 1);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112300, '12-300','Centro de Referencia de Salud Cordillera Oriente','13122', 3, 3);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112301, '12-301','Centro de Salud Familiar Dr. Hernán Alessandri','13123', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112302, '12-302','Centro de Salud Integral Dr. Alfonso Leng','13123', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112303, '12-303','Centro de Salud Familiar Aguilucho','13123', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112304, '12-304','Centro de Salud Familiar Apoquindo','13114', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112306, '12-306','Centro de Salud Familiar Aníbal Ariztía','13114', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112307, '12-307','Centro de Salud Familiar Lo Barnechea','13115', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112309, '12-309','Centro de Salud Familiar Vitacura','13132', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112310, '12-310','Centro de Salud Familiar Rosita Renard','13120', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112311, '12-311','Centro de Salud Familiar Salvador Bustos','13120', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112312, '12-312','Centro de Salud Familiar Félix de Amesti','13118', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112313, '12-313','Centro de Salud Familiar Santa Julia','13118', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112314, '12-314','Centro de Salud Familiar La Faena','13122', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112315, '12-315','Centro de Salud Familiar San Luis','13122', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112316, '12-316','Centro de Salud Familiar Carol Urzúa Ibáñez de Peñalolén','13122', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112317, '12-317','Centro de Salud Familiar La Reina','13113', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112318, '12-318','Centro de Salud Familiar Lo Hermida','13122', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112319, '12-319','Centro de Salud Familiar Padre Alberto Hurtado','13118', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112320, '12-320','Centro de Salud Familiar Juan Pablo II','13113', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112321, '12-321','Centro de Salud Familiar Cardenal Silva Henríquez','13122', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112322, '12-322','Centro de Salud Familiar Padre Gerardo Whelan','13122', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112323, '12-323','Centro Odontológico Macul','13118', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112407, '12-407','Posta de Farellones','13115', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112606, '12-606','COSAM La Reina','13113', 3, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112607, '12-607','COSAM Macul','13118', 3, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112608, '12-608','COSAM Ñuñoa','13120', 3, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112609, '12-609','COSAM Las Condes','13114', 3, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112610, '12-610','COSAM Peñalolén','13122', 3, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112611, '12-611','COSAM Provisam','13123', 3, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112612, '12-612','COSAM Lo Barnechea','13115', 3, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112613, '12-613','COSAM-Vitacura','13132', 3, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112707, '12-707','Centro Comunitario de Salud Familiar Lo Barnechea','13115', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112716, '12-716','Centro Comunitario de Salud Familiar General Carol Urzúa Ibáñez','13122', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112717, '12-717','Centro Comunitario de Salud Familiar Dragones de La Reina','13113', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112799, '12-799','Centro Comunitario de Salud Familiar Bicentenario','13115', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112806, '12-806','SAPU-Aníbal Ariztía','13114', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112807, '12-807','SAPU-Lo Barnechea','13115', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112810, '12-810','SAPU-Rosita Renard','13120', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112813, '12-813','SAPU-Santa Julia','13118', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112814, '12-814','SAPU-La Faena','13122', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112815, '12-815','SAPU-San Luis','13122', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112816, '12-816','SAPU-Carol Urzúa Ibáñez','13122', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112818, '12-818','SAPU-Lo Hermida','13122', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (112820, '12-820','SAPU-Centro de Urgencia Ñuñoa','13120', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (200037, '12-937','Centro Comunitario de Salud Familiar Elena Caffarena','13123', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (200080, '12-901','Centro Odontológico La Reina','13113', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (200142, '12-903','SAPU-La Reina','13113', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (200143, '12-904','SAPU-Aguilucho','13123', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (200188, '12-905','Centro de Especialidades Odontológicas Leng','13123', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (200236, '12-907','Centro Comunitario de Salud Familiar Villa Olímpica','13120', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (200265, '12-908','Centro Comunitario de Salud Familiar Andacollo','13123', 2, 4);

INSERT INTO ESTABLECIMIENTO(id_codigo_nuevo, id_codigo_antiguo, nombre_oficial, COMUNA_id_comuna, TIPO_NIVEL_ATENCION_id_nivel, TIPO_DEPENDENCIA_id_dependencia)
VALUES (200389, '12-923','Centro Comunitario de Salud Familiar Amapolas','13120', 2, 4);