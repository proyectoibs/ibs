
------------------------------------- PARTOS REM24 SECCION A -------------------------------


-------------------------------------------- 1 -----------------------------------------------------------
----------------------------------------------------------- 2018 -----------------------------------------------
--- CONSULTA PARTO CODIGO PRESTACION POR MES(PARTO NORMAL/VAGINAL, DISTOCICO VAGINAL, CESARIA ELECTIVA, CESARIA URGENCIA) ---  
SELECT 'A�O 2018' AS 'PARTOS', r.MES,SUM(CONVERT(int, R.COL01)) AS 'TOTAL' 
FROM dbo.BD_SA_2018 r
WHERE r.CODIGOPRES IN (N'01030100',N'01030200',N'01030300',N'24090700')
GROUP BY r.MES
WITH ROLLUP;  
  

--- CONSULTA PARTO CODIGO PRESTACION (PARTO NORMAL/VAGINAL, DISTOCICO VAGINAL, CESARIA ELECTIVA, CESARIA URGENCIA) ---  
SELECT 'A�O 2018' AS 'PARTOS', 
SUM(CONVERT(int, R.COL01)) AS 'TOTAL' 
FROM dbo.BD_SA_2018 r
WHERE r.CODIGOPRES IN (N'01030100',N'01030200',N'01030300',N'24090700');  

--- 112107 Codigo Hanga Roa ---
--- 112101 Dr. Luis Tisn� B ---
--- CONSULTA PARTO POR HOSPITALES Hanga Roa Luis Tisne---
SELECT e.nombre_oficial AS 'ESTABLECIMIENTO',   
       SUM(CONVERT(int, R.COL01)) AS 'A�O 2018' 
FROM dbo.BD_SA_2018 r INNER JOIN ESTABLECIMIENTO e
ON r.IDESTABLEC = e.id_codigo_nuevo
WHERE r.CODIGOPRES IN (N'01030100',N'01030200',N'01030300',N'24090700') and e.id_codigo_nuevo in ('112107','112101')
GROUP BY e.nombre_oficial;

 ----------------------------------------------------------- 2 ------------------------------------------------
----------------------------------------------------------- 2018 -----------------------------------------------
--- OFICIAL CONSULTA CESARIAS ELECTIVAS Y URGENCIAS POR MES---  
SELECT 'A�O 2018' AS 'CERSARIAS TOTALES',r.MES, SUM(CONVERT(int, R.COL01)) AS 'TOTAL'
FROM dbo.BD_SA_2018 r
WHERE r.CODIGOPRES IN (N'01030300',N'24090700')
GROUP BY R.MES
WITH ROLLUP;

--- OFICIAL CONSULTA CESARIAS ELECTIVAS Y URGENCIAS ---  
SELECT 'A�O 2018' AS 'CESARISA TOTALES',
  SUM(CONVERT(int, R.COL01)) AS 'TOTAL'
FROM dbo.BD_SA_2018 r
WHERE r.CODIGOPRES IN (N'01030300',N'24090700');

--- CONSULTA PARTO POR HOSPITALES Hanga Roa Luis Tisne (CESARIAS ELECTIVAS Y URGENCIAS)---
SELECT e.nombre_oficial AS 'ESTABLECIMIENTO',   
       SUM(CONVERT(int, R.COL01)) AS 'TOTAL' 
FROM dbo.BD_SA_2018 r INNER JOIN ESTABLECIMIENTO e
ON r.IDESTABLEC = e.id_codigo_nuevo
WHERE r.CODIGOPRES IN (N'01030300',N'24090700') and e.id_codigo_nuevo in ('112107','112101')
GROUP BY e.nombre_oficial;


-------------------------------------------------------------- 3 ------------------------------------------------
----------------------------------------------------------- 2018 -----------------------------------------------

--- PORCENTAJE -----

select (28 * 100) / ABS(86)
select  ROUND((1.809 * 100) / ABS(4.725), 0)


----------------------------------------------------------------

